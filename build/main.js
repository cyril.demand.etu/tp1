"use strict";

/* console.log('Welcome to ', {title:'PizzaLand', emoji: '🍕'});
console.warn('Oh oh attention')
console.error('fatal error lol')
//console.clear()
console.table([['Walter','White'],['Skyler','White'],['Saul','Goodman']])
const what = 'door';
debugger;
console.log('Hold', 'the', what );

/* guillemets simples */
//let s1 = 'je suis une chaîne avec des single quotes';

/* ou guillemets doubles */
//let s2 = "je suis une chaîne avec des double quotes";

/* ou accent grave (template strings ES6) */

/*
function getCurrentSchool(){
    return "l'IUT A";
}

let s3 = `Les étudiants de ${ getCurrentSchool() } sont les meilleurs`;
debugger;
console.log(s1);
console.log(s2);
console.log(s3);
*/

/*const name = 'Regina';

const url = "images/" + name.toLowerCase() + ".jpg";

console.log(url);



const html = `<article class="pizzaThumbnail">
                <a href="${url}"> 
                    <img src="${url}"</img> 
                    <section>"${name}</section>
                </a>
            </article>`;

console.log(html);

document.querySelector('.pageContent').innerHTML = html;
*/
var data1 = [{
  name: 'Regina',
  base: 'tomate',
  price_small: 6.5,
  price_large: 9.95,
  image: 'https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300'
}, {
  name: 'Napolitaine',
  base: 'tomate',
  price_small: 6.5,
  price_large: 8.95,
  image: 'https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300'
}, {
  name: 'Spicy',
  base: 'crème',
  price_small: 5.5,
  price_large: 8,
  image: 'https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300'
}];
var html = "";
data1.sort(function (a, b) {
  return a.name - b.name;
});
data1.sort(function (a, b) {
  if (a.price_small - b.price_small != 0) {
    return a.price_small - b.price_small;
  } else {
    return a.price_large - b.price_large;
  }
});

function filtrerParBaseTomate(_ref) {
  var name = _ref.name,
      base = _ref.base,
      price_small = _ref.price_small,
      price_large = _ref.price_large,
      image = _ref.image;

  // Si c'est un nombre
  if (base == "tomate") {
    return true;
  } else {
    return false;
  }
} //const arrByBase = data1.filter(pizza => pizza.base == "tomate");


console.log("après filtre");

function filtrerParPrix6(_ref2) {
  var name = _ref2.name,
      base = _ref2.base,
      price_small = _ref2.price_small,
      price_large = _ref2.price_large,
      image = _ref2.image;

  // Si c'est un nombre
  if (price_small <= 6) {
    return true;
  } else {
    return false;
  }
} // const arrByBase = data1.filter(pizza => pizza.price_small < "tomate");
//const arrByBase =  data1.filter(filtrerParPrix6)


function filtrerParNombreI(_ref3) {
  var name = _ref3.name,
      base = _ref3.base,
      price_small = _ref3.price_small,
      price_large = _ref3.price_large,
      image = _ref3.image;

  // Si c'est un nombre
  if (name.split('i').length == 3) {
    return true;
  } else {
    return false;
  }
} //const arrByBase = data1.filter(pizza => pizza.name.split('i').length >= 3);
//const arrByBase =  data1.filter(filtrerParNombreI)


var arrByBase = data1;
/*const arrByBase = data.map(( { name, base, price_small, price_large, image }) =>{
    return `<article class="pizzaThumbnail">
    <a href="${image}"> 
        <img src="${image}"</img> 
        <section>
            <h4>${name}</h4>
            <ul>
                <li>Prix petit format : ${price_small} €</li>
                <li>Prix grand format : ${price_large} €</li>
            </ul>
        </section>
    </a>
</article>`;
});

document.querySelector('.pageContent').innerHTML = arrByBase.join("");
*/

/* for(let i = 0; i < arrByBase.length; ++i){
    const { name, base, price_small, price_large, image } = arrByBase[i]
    html += `<article class="pizzaThumbnail">
                <a href="${image}"> 
                    <img src="${image}"</img> 
                    <section>
                        <h4>${name}</h4>
                        <ul>
                            <li>Prix petit format : ${price_small} €</li>
                            <li>Prix grand format : ${price_large} €</li>
                        </ul>
                    </section>
                </a>
            </article>`;
} */

arrByBase.forEach(function (_ref4) {
  var name = _ref4.name,
      base = _ref4.base,
      price_small = _ref4.price_small,
      price_large = _ref4.price_large,
      image = _ref4.image;
  html += "<article class=\"pizzaThumbnail\">\n                <a href=\"".concat(image, "\"> \n                    <img src=\"").concat(image, "\"</img> \n                    <section>\n                        <h4>").concat(name, "</h4>\n                        <ul>\n                            <li>Prix petit format : ").concat(price_small, " \u20AC</li>\n                            <li>Prix grand format : ").concat(price_large, " \u20AC</li>\n                        </ul>\n                    </section>\n                </a>\n            </article>");
});
document.querySelector('.pageContent').innerHTML = html;
/* const data = ['Regina', 'Napolitaine', 'Spicy'];

html=data.reduce((html,name) => { 
    const url = `images/${name.toLowerCase()}.jpg`;
    return html + `<article class="pizzaThumbnail">
                <a href="${url}">
                    <img src="${url}"/>
                    <section>${name}</section>
                </a>
            </article>`
},0);

console.log(html);

document.querySelector('.pageContent').innerHTML = html; */
//# sourceMappingURL=main.js.map